from email.policy import default
from random import choices
from django.db import models

from . import const

# Create your models here.

class Motorcycle(models.Model):
  nama = models.CharField(max_length=50)
  merk = models.CharField(max_length=50, choices=const.MERK)
  volume_silinder = models.IntegerField()
  jumlah_silinder = models.IntegerField()
  transmisi = models.CharField(max_length=50, choices=const.TRANSMISI)
  jenis = models.CharField(max_length=50, choices=const.JENIS)
  stok = models.IntegerField(default=0)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  def increaseStok(self):
    self.stok = self.stok + 1

    super(Motorcycle, self).save()
  
  def decreaseStok(self):
    if self.stok > 0:
      self.stok = self.stok - 1

    super(Motorcycle, self).save()

class Sale(models.Model):
  motor_detail = models.ForeignKey(Motorcycle, on_delete=models.CASCADE)
  tahun = models.IntegerField()
  harga_baru = models.BigIntegerField()
  harga_bekas = models.IntegerField()
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)