MERK = (
  ('honda', 'honda'),
  ('yamaha', 'yamaha'),
  ('suzuki', 'suzuki'),
  ('kawasaki', 'kawasaki'),
)

TRANSMISI = (
  ('manual', 'manual'),
  ('otomatis', 'otomatis'),
  ('semi_manual', 'semi_manual'),
)

JENIS = (
  ('matic', 'matic'),
  ('moped', 'moped'),
  ('sport_fairing', 'sport_fairing'),
  ('sport_naked', 'sport_naked'),
  ('trail', 'trail'),
)