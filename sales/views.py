from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q

from skripsifachri.response import errorResponse, successResponse, notFoundResponse
from .models import Motorcycle, Sale
from .serializers import MotorcycleSerializers, SaleSerializers, SaleExpandSerializers

# Create your views here.

# motorcycle
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def createMotorcycle(request):
  serializer = MotorcycleSerializers(data=request.data)

  if serializer.is_valid():
    serializer.save(**serializer.validated_data)
    return successResponse(serializer.data, "Berhasil menambahkan data sepeda motor")

  return errorResponse(serializer.errors)


class OptionMotorcycle(ListAPIView):
    queryset = Motorcycle.objects.filter(~Q(stok=0))
    serializer_class = MotorcycleSerializers
    authentication_class = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('nama', 'merk', 'jenis', 'transmisi')

    def list(self, request, *args, **kwargs):
      result = super().list(request, *args, **kwargs)

      return successResponse(result.data, "Berhasil mendapatkan list sepeda motor")


class ListMotorcycle(ListAPIView):
    queryset = Motorcycle.objects.all()
    serializer_class = MotorcycleSerializers
    authentication_class = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = PageNumberPagination
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('nama', 'merk', 'jenis', 'transmisi')

    def list(self, request, *args, **kwargs):
      result = super().list(request, *args, **kwargs)

      return successResponse(result.data, "Berhasil mendapatkan list sepeda motor")


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def detailMotorcycle(request, id):
  try:
    motorcycle = Motorcycle.objects.get(id=id)
    serializer = MotorcycleSerializers(motorcycle)

    return successResponse(serializer.data, "Berhasil mendapatkan detail sepeda motor")
  except Motorcycle.DoesNotExist:
    return notFoundResponse("Sepeda motor tidak ditemukan")


@api_view(['PUT'])
@permission_classes((IsAuthenticated,))
def editMotorcycle(request, id):
  try:
    motorcycle = Motorcycle.objects.get(id=id)
    serializer = MotorcycleSerializers(motorcycle, data=request.data)

    if serializer.is_valid():
      serializer.save()
      return successResponse(serializer.data, "Berhasil mengubah data sepeda motor")

    return errorResponse(serializer.errors)

  except Motorcycle.DoesNotExist:
    return notFoundResponse("Sepeda motor tidak ditemukan")


@api_view(['DELETE'])
@permission_classes((IsAuthenticated,))
def deleteMotorcycle(request, id):
  try:
    motorcycle = Motorcycle.objects.get(id=id)
    motorcycle.delete()
    return successResponse("", "Berhasil menghapus data sepeda motor")
  except Motorcycle.DoesNotExist:
    return notFoundResponse("Sepeda motor tidak ditemukan")

@api_view(['PUT'])
@permission_classes((IsAuthenticated,))
def increaseStok(request, id):
  try:
    motorcycle = Motorcycle.objects.get(id=id)
    motorcycle.increaseStok()
    return successResponse("", "Berhasil menambah stok sepeda motor")
  except Motorcycle.DoesNotExist:
    return notFoundResponse("Sepeda motor tidak ditemukan")

@api_view(['PUT'])
@permission_classes((IsAuthenticated,))
def decreaseStok(request, id):
  try:
    motorcycle = Motorcycle.objects.get(id=id)
    motorcycle.decreaseStok()
    return successResponse("", "Berhasil mengurangi stok sepeda motor")
  except Motorcycle.DoesNotExist:
    return notFoundResponse("Sepeda motor tidak ditemukan")

# sales
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def createSale(request):
  serializer = SaleSerializers(data=request.data)
  motorcycle = Motorcycle.objects.get(id=request.data['motor_detail'])

  if motorcycle.stok < 1:
    return errorResponse("Stok kosong")

  if serializer.is_valid():
    serializer.save(**serializer.validated_data)
    return successResponse(serializer.data, "Berhasil menambahkan data penjualan")

  return errorResponse(serializer.errors)


class ListSale(ListAPIView):
    queryset = Sale.objects.all().order_by('-created_at')
    serializer_class = SaleExpandSerializers
    authentication_class = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class = PageNumberPagination
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('motor_detail__nama', 'motor_detail__merk', 'motor_detail__jenis', 'motor_detail__transmisi', 'tahun')

    def list(self, request, *args, **kwargs):
      result = super().list(request, *args, **kwargs)

      return successResponse(result.data, "Berhasil mendapatkan list penjualan")


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def detailSale(request, id):
  try:
    sale = Sale.objects.get(id=id)
    serializer = SaleExpandSerializers(sale)

    return successResponse(serializer.data, "Berhasil mendapatkan detail penjualan")
  except Sale.DoesNotExist:
    return notFoundResponse("Penjualan tidak ditemukan")


@api_view(['PUT'])
@permission_classes((IsAuthenticated,))
def editSale(request, id):
  try:
    sale = Sale.objects.get(id=id)
    serializer = SaleSerializers(sale, data=request.data)

    if serializer.is_valid():
      serializer.save()
      return successResponse(serializer.data, "Berhasil mengubah data penjualan")

    return errorResponse(serializer.errors)

  except Sale.DoesNotExist:
    return notFoundResponse("Penjualan tidak ditemukan")


@api_view(['DELETE'])
@permission_classes((IsAuthenticated,))
def deleteSale(request, id):
  try:
    sale = Sale.objects.get(id=id)
    sale.delete()
    return successResponse("", "Berhasil menghapus data penjualan")
  except Sale.DoesNotExist:
    return notFoundResponse("Penjualan tidak ditemukan")

