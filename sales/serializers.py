from dataclasses import field
from rest_framework import serializers

from .models import Motorcycle, Sale

class MotorcycleSerializers(serializers.ModelSerializer):
  class Meta:
    model = Motorcycle
    fields = '__all__'

  def create(self, validated_data):
    motorcycle = Motorcycle.objects.create(**validated_data)
    return motorcycle


class SaleSerializers(serializers.ModelSerializer):
  class Meta:
    model = Sale
    fields = '__all__'

  def create(self, validated_data):
    motor = validated_data['motor_detail']
    motor.decreaseStok()
    sale = Sale.objects.create(**validated_data)
    return sale


class SaleExpandSerializers(serializers.ModelSerializer):
  motor_detail = MotorcycleSerializers(read_only=True)
  
  class Meta:
    model = Sale
    fields = ['id', 'motor_detail', 'tahun', 'harga_baru', 'harga_bekas', 'created_at', 'updated_at']
