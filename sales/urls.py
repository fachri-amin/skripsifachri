from django.urls import path

from . import views

app_name = 'predictor'

urlpatterns = [
    path('motorcycle/create', views.createMotorcycle, name='createMotorcycle'),
    path('motorcycle/option', views.OptionMotorcycle.as_view(), name='optionMotorcycle'),
    path('motorcycle/list', views.ListMotorcycle.as_view(), name='listMotorcycle'),
    path('motorcycle/detail/<int:id>', views.detailMotorcycle, name='detailMotorcycle'),
    path('motorcycle/edit/<int:id>', views.editMotorcycle, name='editMotorcycle'),
    path('motorcycle/delete/<int:id>', views.deleteMotorcycle, name='deleteMotorcycle'),
    path('motorcycle/increase-stok/<int:id>', views.increaseStok, name='increaseStokMotorcycle'),
    path('motorcycle/decrease-stok/<int:id>', views.decreaseStok, name='decreaseStokMotorcycle'),
    
    path('create', views.createSale, name='createSale'),
    path('list', views.ListSale.as_view(), name='listSale'),
    path('detail/<int:id>', views.detailSale, name='detailSale'),
    path('edit/<int:id>', views.editSale, name='editSale'),
    path('delete/<int:id>', views.deleteSale, name='deleteSale'),
]