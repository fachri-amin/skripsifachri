from rest_framework.decorators import api_view
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import authenticate

from .serializers import UserSerializers
from .response import successResponse, errorResponse


@api_view(['POST'])
def get_tokens_for_user(request):
  user = authenticate(username=request.data['username'], password=request.data['password'])

  if not user:
    return errorResponse("", "Username atau password salah")

  refresh = RefreshToken.for_user(user)
  serializer = UserSerializers(user)

  return successResponse({
      'user': serializer.data,
      'token': str(refresh.access_token),
      'refresh_token': str(refresh),
  }, "Berhasil login")