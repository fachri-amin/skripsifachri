from dataclasses import field
from rest_framework import serializers

from django.contrib.auth.models import User

class UserSerializers(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = '__all__'

  def create(self, validated_data):
    user = User.objects.create(**validated_data)
    return user
