from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenRefreshView,
)
from .view import get_tokens_for_user

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/login', get_tokens_for_user, name='token_obtain_pair'),
    path('auth/token/refresh', TokenRefreshView.as_view(), name='token_refresh'),
    path('predictor/', include('predictor.urls', namespace='predictor')),
    path('sales/', include('sales.urls', namespace='sales'))
]
