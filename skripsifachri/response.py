from rest_framework import status
from rest_framework.response import Response

def successResponse(data, message):
  responseData = {
    "success": 1,
    "message": message,
    "data": data,
  }

  return Response(responseData, status=status.HTTP_200_OK)


def errorResponse(data="", message=""):
  responseData = {
    "success": 0,
    "message": message,
    "data": data,
  }

  return Response(responseData, status=status.HTTP_400_BAD_REQUEST)


def notFoundResponse(message=""):
  responseData = {
    "success": 0,
    "message": message,
  }

  return Response(responseData, status=status.HTTP_404_NOT_FOUND)