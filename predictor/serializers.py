from email.policy import default
from random import choices
from rest_framework import serializers

class PredictorSerializer(serializers.Serializer):
  volume_silinder = serializers.IntegerField()
  jumlah_silinder = serializers.IntegerField()
  tahun = serializers.IntegerField()
  harga_baru = serializers.IntegerField()
  merk = serializers.ChoiceField(choices=['honda', 'kawasaki', 'suzuki', 'yamaha'])
  transmisi = serializers.ChoiceField(choices=['manual', 'otomatis', 'semi_manual',])
  jenis = serializers.ChoiceField(choices=['matic', 'moped', 'sport_fairing', 'sport_naked', 'trail'])