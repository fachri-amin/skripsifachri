import joblib
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from skripsifachri.response import errorResponse, successResponse
from .utils import getInputEncoded, transformToRupiahFormat
from .serializers import PredictorSerializer
# Create your views here.

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def getPrediction(request):
  model = joblib.load("predictor/regressor/model.sav")

  serializer = PredictorSerializer(data=request.data)

  if serializer.is_valid():
    encodedData = getInputEncoded(serializer.data)

    price = transformToRupiahFormat(int(model.predict([encodedData])[0][0]))

    return successResponse(
      {
        "price": price,
      },
      "Berhasil memprediksi harga"
    )
    
  return errorResponse(serializer.errors)