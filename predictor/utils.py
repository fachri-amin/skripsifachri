from . import encode

def getInputEncoded(data):
  encodedMerk = []
  encodedTransmisi = []
  encodedJenis = []

  # encoding merk
  if data['merk'] == 'honda':
    encodedMerk = encode.MERK_HONDA
  elif data['merk'] == 'kawasaki':
    encodedMerk = encode.MERK_KAWASAKI
  elif data['merk'] == 'suzuki':
    encodedMerk = encode.MERK_SUZUKI
  elif data['merk'] == 'yamaha':
    encodedMerk = encode.MERK_YAMAHA
  
  # encoding transmisi
  if data['transmisi'] == 'manual':
    encodedTransmisi = encode.TRANMISI_MANUAL
  elif data['transmisi'] == 'otomatis':
    encodedTransmisi = encode.TRANMISI_OTOMATIS
  elif data['transmisi'] == 'semi_manual':
    encodedTransmisi = encode.TRANMISI_SEMI_MANUAL
  
  # encoding jenis
  if data['jenis'] == 'matic':
    encodedJenis = encode.JENIS_MATIC
  elif data['jenis'] == 'moped':
    encodedJenis = encode.JENIS_MOPED
  elif data['jenis'] == 'sport_fairing':
    encodedJenis = encode.JENIS_SPORT_FAIRING
  elif data['jenis'] == 'sport_naked':
    encodedJenis = encode.JENIS_SPORT_NAKED
  elif data['jenis'] == 'trail':
    encodedJenis = encode.JENIS_TRAIL

  encodedData = [
    data['volume_silinder'],
    data['jumlah_silinder'],
    data['tahun'],
    data['harga_baru'],
    *encodedMerk,
    *encodedTransmisi,
    *encodedJenis,
    ]

  return encodedData


def transformToRupiahFormat(value):
    str_value = str(value)
    separate_decimal = str_value.split(".")
    after_decimal = separate_decimal[0]

    reverse = after_decimal[::-1]
    temp_reverse_value = ""

    for index, val in enumerate(reverse):
        if (index + 1) % 3 == 0 and index + 1 != len(reverse):
            temp_reverse_value = temp_reverse_value + val + "."
        else:
            temp_reverse_value = temp_reverse_value + val

    temp_result = temp_reverse_value[::-1]

    return "Rp " + temp_result